# frozen_string_literal: true

require 'socket'

module AniDB::API
    # Represents a connection to the AniDB UDP API.
    class UDPConnection
        attr_reader :imgserver
        attr_reader :nat

        # The default size of the returned packet.
        PACKET_SIZE = 1400

        # Create a new connection.
        #
        # By default the connection wil automatically established.
        #
        # @param  url [String]  the server address
        # @param  port [Integer]  the server port
        # @param  localport [Integer]  the local port to bind to
        # @param  imgserver [Boolean]  use image server
        # @param  nat [Boolean]  include IP and port in server responses
        # @param  autoconnect [Boolean]  automatically connect to server
        def initialize(
            url:         'api.anidb.net',
            port:        9000,
            localport:   9001,
            imgserver:   false,
            nat:         false,
            autoconnect: true
        )
            @connected = false
            @imgserver = imgserver
            @nat       = nat

            connect(url, port, localport) if autoconnect
        end

        # Establish a connection to AniDB.
        #
        # @param  url [String]  the server address
        # @param  port [Integer]  the server port
        # @param  localport [Integer]  the local port to bind to
        # @return  [Boolean] true if connection has been established,
        #          false otherwise
        def connect(url, port, localport)
            return if connected?

            @sock = UDPSocket.new
            @sock.bind(0, localport)
            @sock.connect(url, port)
            @connected = true
        rescue Errno::EADDRINUSE
            # TODO: handle error
            false
        else
            true
        end

        # Disconnect the current socket.
        #
        # @return  [Boolean]  true if connection has been closed,
        #          false otherwise
        def disconnect
            return unless connected?

            @sock.shutdown
            @sock.close
            @connected = false
        rescue Errno::EADDRINUSE
            # TODO: handle error
            false
        else
            true
        end

        # Send a message to the server.
        #
        # @param  msg [String]  the message to send
        # @return  [Array]  the server response
        def raw_request(msg)
            raise 'requires active connection' unless connected?
            raise ArgumentError, 'cannot convert msg into string' unless msg.respond_to? :to_s

            @sock.send(msg.to_s, 0)
            @sock.recvfrom(PACKET_SIZE)
        end

        alias << raw_request

        # Check if the connection has been established.
        def connected?
            @connected
        end
    end
end
