# frozen_string_literal: true

module AniDB::API
    class UDPResponse
        attr_reader :tag
        attr_reader :status
        attr_reader :msg

        def initialize(msg)
            @data = {}

            @tag    = nil
            @status = nil
            @lines  = msg.split("\n")
            @msg    = @lines.shift

            apply_filters
        end

        def [](key)
            @data[key]
        end

        private

        def apply_filters
            filter_tag
            filter_status
            filter_error_message
            filter_nat_address
            filter_tokens
            filter_notify_packet_id
            filter_buddy_pager
        end

        def filter_tag
            match = /^(?<tag>.*) (?<tail>\d{3} .*$)/ =~ @msg
            return if match.nil?

            @tag = tag
            @msg = tail
        end

        def filter_status
            match = /^(?<status>\d{3}) (?<tail>.*$)/ =~ @msg
            return if match.nil?

            @status = status.to_i
            @msg    = tail
        end

        def filter_error_message
            case @status
            when 504
                @msg, _, @data[:error] = @msg.rpartition(' - ')
            when 555
                @data[:error] = @lines.first
            end
        end

        def filter_tokens
            return unless [200, 201, 209].include? @status

            match = /^(?<token>.*) (?<tail>(LOGIN|ENCRYPTION) .*$)/ =~ @msg
            return if match.nil?

            case @msg
            when /LOGIN/
                @data[:token] = token
                @msg = tail
            when /ENCRYPTION/
                @data[:salt] = token
                @msg = tail
            end
        end

        def filter_nat_address
            return unless [200, 201].include? @status

            match = /^(?<before>.*) (?<addr>(\d{1,3}\.){3}\d{1,3}:\d{1,5}) (?<after>.*)$/ =~ @msg
            return if match.nil?

            addr            = addr.split(':')
            @data[:address] = addr.first
            @data[:port]    = addr.last.to_i
            @msg = "#{before} #{after}"
        end

        def filter_notify_packet_id
            return unless [720, 753, 794, 799].include? @status

            match = /^(?<id>\d+) (?<rest>.*$)/ =~ @msg
            return if match.nil?

            @data[:notify_packet_id] = id.to_i
            @msg = rest
        end

        def filter_buddy_pager
            return unless [253, 254].include? @status

            match = /^(?<pages>\d+ \d+ \d+) (?<rest>.*$)/ =~ @msg
            return if match.nil?

            @data[:buddy_pager] = pages.split(' ').map(&:to_i)
            @msg = rest
        end
    end
end
