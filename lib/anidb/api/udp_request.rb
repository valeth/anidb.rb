# frozen_string_literal: true

module AniDB::API
    # Represents a request to the AniDB UDP API.
    class UDPRequest
        attr_reader :cmd
        attr_reader :args
        attr_reader :auth

        def initialize(cmd, args: {}, auth: true)
            @cmd  = cmd
            @args = args
            @auth = auth
        end

        def to_s
            @msg ||= [@cmd, parse_args(@args)].compact.join(' ')
        end

        private

        def escape_args
            @args.map do |k, v|
                "#{k}=#{v.to_s.sub('&', '&amp;')}"
            end
        end

        def parse_args
            escape_args.join('&') unless @args.empty?
        end
    end
end
