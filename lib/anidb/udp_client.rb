# frozen_string_literal: true

module AniDB
end

require_relative 'api/udp_connection'
require_relative 'api/udp_request'
require_relative 'api/udp_response'

module AniDB
    # An AniDB client using the UDP API.
    class UDPClient
        def initialize
            # TODO: stub
        end
    end
end
