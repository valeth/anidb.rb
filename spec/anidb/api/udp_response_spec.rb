# frozen_string_literal: true

require 'spec_helper'
require 'yaml'
require 'anidb/udp_client'

RESPONSES = YAML.load_file('./spec/fixtures/api/udp_responses.yml')
KEYS = [
    :token,
    :salt,
    :error,
    :address,
    :port,
    :notify_packet_id
].freeze

RSpec.describe AniDB::API::UDPResponse do
    RESPONSES.each do |cmd, responses|
        it "parses responses for #{cmd}" do
            responses.each do |response, check|
                res = AniDB::API::UDPResponse.new(response)
                expect(res.tag).to     eq(nil)
                expect(res.status).to  eq(check['status'])
                expect(res.msg).to     eq(check['msg'])

                KEYS.each do |x|
                    expect(res[x]).to eq(check[x.to_s])
                end

                res_tag = AniDB::API::UDPResponse.new("mytag #{response}")
                expect(res_tag.tag).to     eq('mytag')
                expect(res_tag.status).to  eq(check['status'])
                expect(res_tag.msg).to     eq(check['msg'])

                KEYS.each do |x|
                    expect(res_tag[x]).to eq(check[x.to_s])
                end
            end
        end
    end
end
