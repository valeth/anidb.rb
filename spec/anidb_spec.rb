# frozen_string_literal: true

require 'spec_helper'

RSpec.describe AniDB do
    it 'has a version number' do
        expect(AniDB::VERSION).not_to be nil
    end
end
