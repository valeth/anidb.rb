# coding: utf-8
# frozen_string_literal: true

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'anidb/version'

Gem::Specification.new do |spec|
    spec.name = 'anidb'
    spec.version       = AniDB::VERSION
    spec.authors       = ['Valeth']
    spec.email         = ['patrick.auernig@gmail.com']

    spec.summary       = 'AniDB API for Ruby'
    spec.homepage      = 'https://gitlab.com/valeth/anidb.rb'
    spec.license       = 'GPL-3.0'

    spec.files         = `git ls-files -z`.split("\x0").reject do |f|
        f.match(%r{^(test|spec|features)/})
    end
    spec.bindir        = 'exe'
    spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
    spec.require_paths = ['lib']

    spec.required_ruby_version = '>= 2.3.0'

    spec.add_dependency 'digest-ed2k-hash', '~> 0'

    spec.add_development_dependency 'bundler', '~> 1.14'
    spec.add_development_dependency 'rake',    '~> 10.0'
    spec.add_development_dependency 'rspec',   '~> 3.0'
    spec.add_development_dependency 'yard',    '~> 0.9'
end
